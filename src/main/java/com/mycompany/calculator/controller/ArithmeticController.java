/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calculator.controller;

import com.mycompany.calculator.domain.Command;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Wut
 */
public class ArithmeticController {
    private int currentResult = 0;
    private char operator;
    private int operand;
    
    private static ArithmeticController instance = new ArithmeticController();
    private List<Command> commandList;
    private ArithmeticController(){
        currentResult = 0;
        commandList = Collections.synchronizedList(new ArrayList<Command>());
    }
    public int compute(){
        if((operator != ' ') || (operand != 0)){
            Command command = new Command('+', currentResult,this.operand);
            commandList.add(command);
            int result = currentResult;
            switch(operator){
                case '+':{
                    result = currentResult + operand;
                    break;
                }
                case '-':{
                    result = currentResult - operand;
                    break;
                }
                default:
                    break;
            }
            return result;
        }else
            throw new Exception("Incomplete Operation");
        
    }
    public int add(int operand){
        
    }
}
