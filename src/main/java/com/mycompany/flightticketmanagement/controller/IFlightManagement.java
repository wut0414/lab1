/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticketmanagement.controller;

import com.mycompany.flighttticketmangement.domain.Flight;
import com.mycompany.flighttticketmangement.domain.FlightForTrip;

import com.mycompany.flighttticketmangement.domain.Trip;
import java.util.List;

/**
 *
 * @author Wut
 */
public interface IFlightManagement {
    List<FlightForTrip> searchFlight(Trip trip) throws MissingRequiredTripInfoException;
    
}
