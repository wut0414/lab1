/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flighttticketmangement.domain;

/**
 *
 * @author Wut
 */
public enum AirLine {
        THAI_AIRWAY("Thai Airways International");
        
        private String name;
        
        private AirLine(String name)
        {
             this.name = name;
        }
        
        public String getName()
        {
             return name;
        }
}
