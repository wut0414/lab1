/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flighttticketmangement.domain;

/**
 *
 * @author Wut
 */
public enum Airport {
    BKK("Suvanabhumi Airport","Bankok(Bang Phli,Samut Prakan","TH"),
    DMK("Don Mueang International Airport","Bangkok","TH"),
    HKT("Phuket International Airoort","Phuket","TH"),
    CDG("Charles-de-Gaulle","Paris","FR"),
    CNX("Chiang Mai International Airport","Chiang Mai","TH");
    
    private final String airportName;
    private final String cityName;
    private final String countryInitial;
    
    Airport(String airportName,String cityName,String countryInitial){
        this.airportName = airportName;
        this.cityName = cityName;
        this.countryInitial = countryInitial;
    }     
    public String getAirportName(){
        return airportName;
    }       
    public String getCityName(){
        return cityName;
    }
    public String getCountryInitial(){
        return countryInitial;
    }
}


