/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flighttticketmangement.domain;

import java.time.ZonedDateTime;

/**
 *
 * @author Wut
 */
public class Flight {
    
         private String flightNumber;
         private AircraftType aircraftType;
         private AirLine operator;
         private ZonedDateTime departureTime;
         private ZonedDateTime arrivalTime;
         private Airport fromAirport;
         private Airport toAirport;
         private boolean inBound;
         private int remainingSeats;
         private String departureTerminal;
         private String arrivalTerminal;
         private int numberOfTerminal;
         private FareRate fare;

    public String getFlightNumber() {
        return flightNumber = "TG930";
    }

    public AircraftType getAircraftType() {
        return aircraftType  = AircraftType.A320;
    }

    public AirLine getOperator() {
        return operator = AirLine.THAI_AIRWAY;
    }

    public ZonedDateTime getDepartureTime() {
        return departureTime = ZonedDateTime.parse("2007-12-03T10:15:30+01:00[Europe/Paris]");
    }

    public ZonedDateTime getArrivalTime() {
        return arrivalTime = ZonedDateTime.parse("2007-12-03T10:15:30+01:00[Europe/Paris]");
    }

    public Airport getFromAirport() {
        return fromAirport = Airport.BKK;
    }

    public Airport getToAirport() {
        return toAirport = Airport.CDG;
    }

    public boolean isInBound() {
        return inBound;
    }

    public int getRemainingSeats() {
        return remainingSeats = 50;
    }

    public String getDepartureTerminal() {
        return departureTerminal = "Terminal 4";
    }

    public String getArrivalTerminal() {
        return arrivalTerminal = "Terminal 4";
    }

    public int getNumberOfTerminal() {
        return numberOfTerminal;
    }

    public FareRate getFare() {
        return fare = FareRate.C;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setAircraftType(AircraftType aircraftType) {
        this.aircraftType = aircraftType;
    }

    public void setOperator(AirLine operator) {
        this.operator = operator;
    }

    public void setDepartureTime(ZonedDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public void setArrivalTime(ZonedDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public void setFromAirport(Airport fromAirport) {
        this.fromAirport = fromAirport;
    }

    public void setToAirport(Airport toAirport) {
        this.toAirport = toAirport;
    }

    public void setInBound(boolean inBound) {
        this.inBound = inBound;
    }

    public void setRemainingSeats(int remainingSeats) {
        this.remainingSeats = remainingSeats;
    }

    public void setDepartureTerminal(String departureTerminal) {
        this.departureTerminal = departureTerminal;
    }

    public void setArrivalTerminal(String arrivalTerminal) {
        this.arrivalTerminal = arrivalTerminal;
    }

    public void setNumberOfTerminal(int numberOfTerminal) {
        this.numberOfTerminal = numberOfTerminal;
    }

    public void setFare(FareRate fare) {
        this.fare = fare;
    }
         
    
}
